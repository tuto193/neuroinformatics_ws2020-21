# Script for first Assignment

- Today we're going to show you that the Binomial distribution is a member of the exponential family
- This basically means that the Binomial Distribution, which has this form:
$$Bin(x|N,\mu) = {N\choose x}\mu^x\cdot (1-\mu)^{N-x}$$
- Has to be able to fit into the formula of the exponential family distribution:
$$p(x|\eta)=h(x)\cdot g(\eta)\cdot e^{(\eta^T\cdot u(x))}$$
- At first it might sound counterintuitive to just form a formula to one that is even more complex
- But for the exponential distribution there is more to it than just some math training
- It's called the exponential family, because almost all commonly used distributions are a member of said family

** Example with Bernoulli (first the actual distribution, then show parameters and formula for exponential family distribution and plot it as well) **
- Bernoulli Distribution <br>
$p(x|\mu) = \mu^x (1-\mu)^{1-x}$ <br>
$u(x) = x$ <br>
$h(x) = 1$ <br>
$g(\eta) = 1 - \sigma(\eta) = \sigma(-\eta)$ with $\mu=\sigma(\eta)=\frac{1}{1+e^{-n}}$<br>
$p(x|\eta)=\sigma(-\eta)e^{\eta x}$
**

- If we now want to prove a certain behaviour or develop a new process for the Bernoulli distribution, we could just do it of course, but because we know that the Bernoulli Distribution is part of the exponential family distribution, we can also decide to do it for the exponential family altogether. 
- And now it starts to make sense why we use them: They give us a general approach to working with the most common distributions and save us a lot of time by proving something once for all distributions that are part of the family

---
# The actual assignment
- But lets get into the actual assignment here:
- We need to reform the binomial distribution to the distribution of the exponential family
- It is noteworhy that N (the number of trials) is fixed

$${N\choose x}\mu^x\cdot (1-\mu)^{N-x}$$
- In the current form we can't really do anything with the formula, so we have to put it into a form where we can work with it, and that form would be a logarithm, beacause inside of logarithms we can't just get rid of exponents, but we can also turn the products into sums
---
- We can turn this formula into a logarithm because the exponential and ln cancel each other out($x = exp(ln(x))$)
- Nothing comes for free so we have to pay the price of having an exponential as well, but this should be no problem

$$exp(ln({N\choose x}\mu^x\cdot (1-\mu)^{N-x}))$$
---
- Now we use some logarithm rules:
- We first split the logarithm (which also converts the products into sums) ($ln(x\cdot y) = ln(x) + ln(y)$)

$$exp(ln({N\choose x}) + ln(\mu^x) + ln((1-\mu)^{N-x}))$$
- Now every term is in its own logarithm, which allows us to get rid of the exponents
---
- We prepend all exponents inside the logarithms to their respective logarithms ($ln(x^y) = y \cdot ln(x)$)

$$exp(ln({N\choose x}) + x \cdot ln(\mu) + (N-x) \cdot ln(1-\mu)$$
- We don't have many choices agains
---
- So lets multiply the last two terms
  
$$exp(ln({N\choose x}) + x \cdot ln(\mu) + N \cdot ln(1-\mu) - x \cdot ln(1-\mu)$$
- The trend continues...
---
- Lets change the order a bit to make it clearer what we can try next
  
$$exp(ln({N\choose x}) + x \cdot ln(\mu) - x \cdot ln(1-\mu) + N \cdot ln(1-\mu)$$
- We can get the x out of the second and third term

$$exp(ln({N\choose x}) + x \cdot(ln(\mu) - ln(1-\mu)) + N \cdot ln(1-\mu)$$
---
- And here comes the next logarithm rule: $ln(x)-ln(y)=ln(\frac{x}{y})$. So lets apply it

$$exp(ln({N\choose x}) + x \cdot ln(\frac{\mu}{1-\mu}) + N \cdot ln(1-\mu)$$
---
- Now we can make that exponential a little smaller ($exp(x+y) = exp(x) \cdot exp(y)$)

$$exp(ln({N\choose x})) \cdot exp(x \cdot ln(\frac{\mu}{1-\mu})) \cdot exp(N \cdot ln(1-\mu))$$
- The binomial can also be freed from 2/3 of its brackets, because as we now know exponential and ln cancel each other out

$${N\choose x} \cdot exp(x \cdot ln(\frac{\mu}{1-\mu})) \cdot exp(N \cdot ln(1-\mu))$$
- and with two final order swappings you should see that we are done:

$${N\choose x} \cdot exp(N \cdot ln(1-\mu)) \cdot exp(ln(\frac{\mu}{1-\mu}) \cdot x)$$
- for comparison:
$$h(x)\cdot g(\eta)\cdot e^{(\eta^T\cdot u(x))}$$
---
- Now we only have to specify the the parameters of the exponential distribution directly
- The first parameter is the underlying measure: $h(x)={N\choose x}$
  - Its quite easy pick because it is the only term that is onyl dependent on x and not $\mu$ (these are the only free parameters in the formula)

*** NICHT SICHER
- The natural parameter: $\eta =ln(\frac{\mu}{1-\mu})$
  - For this we picked the only term inside an exponential, that is mulipiled with a term that contains x
  - We need another notation for $\mu$ because g is dependent on $\eta$
  - Therefor we solve $\eta$ for $\mu$: $\mu = (\frac{e^{\eta}}{e^{\eta}+1})$


- $g(\eta)=exp(N \cdot ln(1-\mu))$
  - This term was picked because it was the only one inside an exponential that is only dependent on $\mu$


*** Ich habe keinen Plan wie $\mu = \sigma(\eta)$ errechnet wurde. 

- The sufficient statistic: $u(x)=x$
  - This is always the term that is multiplied with the natural parameter in the exponential