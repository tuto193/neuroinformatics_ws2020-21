from manimlib.imports import *
import os
import pyclbr
from scipy.stats import bernoulli
import numpy as np


class Bernoulli(GraphScene):
    CONFIG = {
        "x_min" : -0.5,
        "x_max" : 1.5,
        "y_min" : 0,
        "y_max" : 3,
        "graph_origin" : 2*DOWN,
        "function_color" : RED,
        "axes_color" : WHITE,
        # "x_labeled_nums" : np.arange(-0.5, 1.6, 0.25),
        # "y_labeled_nums" : np.arange(0, 3.1, 0.25)
    }
    def construct(self):
        self.setup_axes(animate=True)
        func_graph=self.get_graph(self.func_p_from_x_mu,self.function_color)
        # graph_lab = self.get_graph_label(
        #     func_graph,
        #     label = r"p(x|\mu) = \mu^x (1-\mu)^{1-x}",
        #     color=GREEN,
        # )
        # label_coord = self.input_to_graph_point(TAU,func_graph)
        # two_pi.next_to(label_coord,RIGHT+UP)
        graph_lab = TexMobject(r"p(x|\mu) = \mu^x (1-\mu)^{1-x}")
        graph_lab.shift(LEFT * 3)

        self.play(ShowCreation(func_graph))
        self.play(ShowCreation(graph_lab))
        self.wait(6)

    def func_p_from_x_mu(self, x):
        return bernoulli.var(x)

class MyEquation(Scene):
    # A short script showing how to use Latex commands

    def draw_3_to_10(self):
        # 2.
        eq2_1r = r"Bin(x|N,\mu)"
        eq2_2r = r"{N\choose x}\mu^x\cdot (1-\mu)^{N-x}"
        eq2_full = TexMobject(f"{eq2_1r} = {eq2_2r}")


        eq2_full.shift(UP)
        self.play(Write(eq2_full))
        self.wait(2) # uncomment
        ## 2 DONE


        ## 3.
        eq3_1r = r"p(x|\eta)"
        eq3_2r = r"h(x)"
        eq3_3r = r"g(\eta)"
        eq3_4r = r"e^{(\eta^T\cdot u(x))}"
        cdot = r"\cdot"
        eq3_full = TexMobject(r"p(x|\eta)=","h(x)", r"\cdot", r"g(\eta)", r"\cdot", r"e^{(\eta^T\cdot u(x))}")
        eq3_full.set_color_by_tex_to_color_map({
            "h(x)": RED,
            r"g(\eta)": BLUE,
            "u(x)": YELLOW,
        })


        eq3_full.shift(DOWN)
        self.play(Write(eq3_full))
        self.wait(3) # uncomment
        self.play(FadeOut(eq3_full))
        eq3_full.shift(UP*4 + LEFT*2)

        self.play(FadeOut(eq2_full))
        self.play(FadeInFromDown(eq3_full))
        self.wait(2) # uncomment
        ## 3 DONE

        ## 6.
        eq6_bern1 = TexMobject(r"p(x|\mu) = \mu^x(1 - \mu)^{1 - x}")
        eq6_geta = TexMobject(r"g(\eta) = 1 - \sigma(\eta)", r"= \sigma(-\eta)", r"\text{ with } \mu =", r" \sigma(\eta) = \frac{1}{1+e^{-\eta}}").set_color(BLUE)
        eq6_ux = TexMobject(r"u(x) = x").set_color(YELLOW)
        eq6_hx = TexMobject(r"h(x) = 1").set_color(RED)
        eq6_pxeta = TexMobject(r" \Rightarrow p(x|\eta) = \sigma(-\eta)e^{\sigma(\eta) \cdot x}")


        eq6_bern1.shift(3*LEFT + 2*UP) # top left
        eq6_hx.next_to(eq6_bern1, DOWN) # second on list
        # eq6_geta.shift()
        eq6_ux.shift(DOWN+3*LEFT)
        eq6_pxeta.shift(2*DOWN)
        self.play(Write(eq6_bern1))
        self.play(Write(eq6_hx))
        self.wait(1)
        self.play(Write(eq6_geta))
        self.wait(1)
        self.play(Write(eq6_ux))
        self.wait(1)
        self.play(Write(eq6_pxeta))
        self.wait(3)
        self.play(FadeOut(eq6_bern1))
        self.play(FadeOut(eq6_hx))
        self.play(FadeOut(eq6_geta))
        self.play(FadeOut(eq6_ux))
        self.play(FadeOut(eq6_pxeta))
        ## Back to main problem
        self.play(FadeOut(eq3_full))


    def draw_11_to_24(self):
        11 # ENDED WITH eq3_full fading out
        eq11_1r = eq2_2r
        eq11_1 = TexMobject(eq11_1r)
        eq11_2 = TexMobject("=")
        # eq11_3r =
        eq11_3 = TexMobject(
            r"exp\left(",
            "ln",
            r"\left(",
            r"{N\choose x}\mu^x", r"\cdot",  r"(1-\mu)^{N-x} ",
            r"\right) \right)"
        ).set_color_by_tex_to_color_map({
            "ln": YELLOW,
            r"\cdot": BLUE,
        })
        # eq11_1r.shift(UP*2+3*LEFT)

        eq11_1.shift(UP+3*LEFT)
        eq11_2.shift(DOWN)
        eq11_2.align_to(eq11_1, LEFT)
        eq11_3.next_to(eq11_2, RIGHT)
        eq11_bottom = VGroup(eq11_2, eq11_3)
        eq11_useless = VGroup(eq11_1, eq11_2)

        self.play(FadeIn(eq11_1))
        self.wait(2)
        self.play(FadeIn(eq11_bottom))
        self.wait(2)

        self.play(FadeOut(eq11_useless))
        self.play(ApplyMethod(eq11_3.shift, 3*UP))
        self.wait(3)
        # color ln and ·
        # 16.
        eq16_1 = TexMobject(
            r"exp \left(",
            "ln",
            r"\left({N\choose x} \right)",
            "+",
            "ln",
            "(\mu", "^x", ") ",
            "+",
            "ln",
            r"\left((1-\mu)", "^{N-x}", r"\right) \right)"
        ).set_color_by_tex_to_color_map({
            "ln": YELLOW,
            "+": BLUE,
        })
        eq16_1.next_to(eq11_3, DOWN)
        self.play(FadeInFromDown(eq16_1))
        self.wait(3)
        self.play(FadeOut(eq11_3))
        self.wait(3)
        eq16_1.set_color_by_tex_to_color_map({
            "ln": WHITE,
            "+": WHITE,
            "^x": GREEN,
            "^{N-x}": GREEN,
        })
        self.play(ApplyMethod(eq16_1.shift, 2*UP))

        eq18_1 = TexMobject(
            r"exp \left(ln\left({N\choose x} \right) + ",
            r"x \cdot",
            r"ln(\mu) + ",
            r"(N-x) \cdot",
            r"ln(1-\mu)",
            r"\right)"
        ).set_color_by_tex_to_color_map({
            r"x \cdot": GREEN,
            r"(N-x) \cdot": GREEN,
        })
        eq18_1.next_to(eq16_1, DOWN)

        self.play(FadeInFromDown(eq18_1))
        self.wait(2)
        self.play(FadeOut(eq16_1))
        self.wait(2)
        self.play(ApplyMethod(eq18_1.shift, 2*UP))
        # 20. So lets multiply the last two terms
        eq18_1.set_color_by_tex_to_color_map({
            r"x \cdot": WHITE,
            r"(N-x) \cdot": RED,
            r"ln(1-\mu)": YELLOW,
        })

        eq20_1 = TexMobject(
            r"exp \left(ln \left({N\choose x} \right) + x \cdot ln(\mu)",
            r"+N \cdot",
            r"ln(1-\mu)",
            r"- x \cdot",
            r"ln(1-\mu)",
            r"\right)"
        ).set_color_by_tex_to_color_map({
            r"+N \cdot": RED,
            r"- x \cdot": RED,
            r"ln(1-\mu)": YELLOW,
        })

        eq20_1.next_to(eq18_1, DOWN)
        eq20_1.align_to(eq18_1, LEFT)
        self.play(FadeInFromDown(eq20_1))
        self.wait(2)
        self.play(FadeOut(eq18_1))
        self.wait(2)
        eq20_1.set_color_by_tex_to_color_map({
            r"- x \cdot": RED,
            r"+N \cdot": GREEN,
            r"ln(1-\mu)": WHITE,
        })

        eq22_1 = TexMobject(
            r"exp \left(ln \left({N\choose x} \right)",
            r"+ x \cdot ln(\mu)",
            r"- x \cdot ln(1-\mu)",
            r"+ N \cdot ln(1-\mu)",
            r"\right)"
        ).set_color_by_tex_to_color_map({
            r"- x \cdot ln(1-\mu)": RED,
            r"+ N \cdot ln(1-\mu)": GREEN,
        })
        eq22_1.align_to(eq20_1, LEFT)
        self.play(Transform(eq20_1, eq22_1))
        self.wait(2)
        self.play(FadeOut(eq20_1))
        self.play(ApplyMethod(eq22_1.shift, UP))
        self.wait(2)
        eq22_1.set_color_by_tex_to_color_map({
            r"+ x \cdot ln(\mu)": BLUE,
            r"- x \cdot ln(1-\mu)": BLUE,
            r"+ N \cdot ln(1-\mu)": WHITE,
        })

        # 23. We can get the x out of the second and third term
        eq23_1 = TexMobject(
            r"exp",
            r"\left(ln \left({N\choose x} \right)",
            "+",
            r"x \cdot",
            r"\left(ln(\mu) - ln(1-\mu) \right)",
            "+",
            r"N \cdot ln(1-\mu) \right)"
        ).set_color_by_tex_to_color_map({
            r"x \cdot":BLUE,
            r"\left(ln(\mu) - ln(1-\mu) \right)": BLUE,
        })

        eq23_1.align_to(eq22_1, LEFT)
        eq23_1.next_to(eq22_1, DOWN)
        self.play(Write(eq23_1))
        self.wait(2)
        self.play(FadeOut(eq22_1))
        self.play(ApplyMethod(eq23_1.shift, 2*UP))
        eq23_1.set_color_by_tex_to_color_map({
            r"x \cdot": WHITE,
            r"\left(ln(\mu) - ln(1-\mu) \right)": YELLOW,
        })
        # self.wait(2)
        eq24_1 = TexMobject(
            "exp",
            r"\left(ln \left({N\choose x} \right)",
            "+",
            r"x \cdot",
            r"ln\left(\frac{\mu}{1-\mu}\right)",
            "+",
            r"N \cdot ln(1-\mu) \right)",
        )
        # eq24_1.set_color_by_tex_to_color_map({
        #     r"ln\left(\frac{\mu}{1-\mu}\right)": YELLOW,
        # })
        # eq24_1.align_to(eq23_1, LEFT)
        # eq24_1.next_to(eq23_1, DOWN)
        # self.play(Write(eq24_1))
        # self.wait(2)
        # self.play(FadeOut(eq23_1))
        # self.play(ApplyMethod(eq24_1.shift, UP))
        # self.wait(2)

        eq24_1.set_color_by_tex_to_color_map({
            r"ln\left(\frac{\mu}{1-\mu}\right)": WHITE,
            "exp": RED,
            "+": RED,
        })


    def draw_25_to_39(self):
        # 25. Now we can make that exponential a little smaller
        eq25_1 = TexMobject(
            r"exp",
            r"\left(ln \left({N\choose x} \right) \right)",
            r"\cdot",
            r"exp",
            r"\left(x \cdot ln \left(\frac{\mu}{1-\mu} \right) \right)",
            r"\cdot",
            r"exp",
            r"\left(N \cdot ln(1-\mu) \right)"
        ).set_color_by_tex_to_color_map({
            r"exp": RED,
            r"\cdot": RED,
        })

        eq24_1.shift(2*UP+ LEFT)
        self.play(Write(eq24_1))

        self.present_down_disappear_push_up(eq24_1, eq25_1)

        eq25_1.set_color_by_tex_to_color_map({
            r"exp": WHITE,
            r"\cdot": WHITE,
            r"\left(ln \left({N\choose x} \right) \right)": RED,
        })

        eq26_1 = TexMobject(
            r"{N\choose x}",
            r"\cdot",
            r"exp \left(x \cdot ln \left(\frac{\mu}{1-\mu} \right) \right)",
            r"\cdot",
            r"exp \left(N \cdot ln(1-\mu) \right)"
        ).set_color_by_tex_to_color_map({
            r"{N\choose x}": RED,
        })

        self.present_down_disappear_push_up(eq25_1, eq26_1)
        eq26_1.set_color_by_tex_to_color_map({
            r"{N\choose x}": WHITE,
            r"exp \left(x \cdot ln \left(\frac{\mu}{1-\mu} \right) \right)": BLUE,
            r"exp \left(N \cdot ln(1-\mu) \right)": GREEN,
        })

        eq27_1 = TexMobject(
            r"{N\choose x}",
            r"\cdot",
            r"exp(N \cdot ln(1-\mu))",
            r"\cdot",
            r"exp\left(",
            r"ln\left(\frac{\mu}{1-\mu}\right)",
            r"\cdot x",
            r"\right)"
        ).set_color_by_tex_to_color_map({
            r"exp(N \cdot ln(1-\mu))": GREEN,
            r"exp\left(ln\left(\frac{\mu}{1-\mu}\right) \cdot x\right)": BLUE,
        })

        self.present_down_disappear_push_up(eq26_1, eq27_1)
        eq27_1.shift(2*UP)
        eq27_1.set_color(WHITE)
        self.add(eq27_1)

        eq28_1 = TexMobject(r"h(x)")
        eq28_1.next_to(eq27_1, DOWN)
        eq28_1.align_to(eq27_1, LEFT)
        eq28_2 = TexMobject(r"\cdot")
        eq28_2.next_to(eq28_1, RIGHT)
        eq28_3 = TexMobject(r"g(",r"\eta",")")
        eq28_3.next_to(eq28_2, RIGHT)
        eq28_4 = TexMobject(r"\cdot")
        eq28_4.next_to(eq28_3, RIGHT)
        eq28_5 = TexMobject(
            r"e^{(",
            r"\eta^T",
            r"\cdot", "u(x)", ")}",
        )
        eq28_5.next_to(eq28_4, RIGHT)

        self.add(eq28_1)
        self.add(eq28_2)
        self.add(eq28_3)
        self.add(eq28_4)
        self.add(eq28_5)

        eq29_1 = TexMobject(
            r"h(x)={N\choose x}"
        ).set_color(BLUE)
        eq29_1.next_to(eq28_5, DOWN)
        eq29_1.align_to(eq27_1, LEFT)

        eq28_1.set_color(BLUE)
        self.play(FadeInFromDown(eq29_1))
        self.wait(2)
        eq28_1.set_color(WHITE)
        eq28_3.set_color_by_tex_to_color_map({
            r"\eta": YELLOW,
        })
        eq28_5.set_color_by_tex_to_color_map({
            r"\eta^T": YELLOW,
        })
        eq30_1 = TexMobject(
            r"\eta =ln\left(\frac{\mu}{1-\mu}\right)"
        ).set_color(YELLOW)
        self.present_down_disappear_push_up(eq29_1, eq30_1)
        eq33_1 = TexMobject(
            r"ln\left(",
            r"\frac{\mu}{1-\mu}",
            r"\right)=\eta",
        ).set_color(YELLOW)
        eq33_1.set_color_by_tex_to_color_map({
            r"\frac{\mu}{1-\mu}": BLUE,
        })
        eq33_2 = TexMobject(
            r"| e^{()}",
        ).set_color(GREEN)
        self.present_down_disappear_push_up(eq30_1, eq33_1)
        eq33_2.next_to(eq33_1, RIGHT)
        self.play(FadeIn(eq33_2))
        self.play(FadeOut(eq33_2))

        eq34_1 = TexMobject(
            r"\frac{\mu}{1-\mu}",
            r"=e^{\eta}",
        ).set_color(YELLOW)
        self.present_down_disappear_push_up(eq33_1, eq34_1)

        eq34_2 = TexMobject(
            r"| \cdot (1-\mu)",
        ).set_color(BLUE)
        eq34_2.next_to(eq34_1, RIGHT)
        self.play(Write(eq34_2))
        self.wait(2)
        self.play(FadeOut(eq34_2))

        eq35_1 = TexMobject(
            r"\mu =",
            r"e^\eta",
            r"\cdot (1-\mu)",
        ).set_color(YELLOW)

        self.present_down_disappear_push_up(eq34_1, eq35_1)
        eq35_1.set_color_by_tex_to_color_map({
            r"e^\eta": GREEN,
        })

        eq35_2 = TexMobject(
            r"\mu=",
            r"e^\eta",
            r"-\mu\cdot",
            r"e^\eta",
        ).set_color(YELLOW)
        eq35_2.set_color_by_tex_to_color_map({
            r"e^\eta": GREEN,
        })

        self.present_down_disappear_push_up(eq35_1, eq35_2)
        self.play(ApplyMethod(eq35_2.shift, UP)) # it was a bit too down

        eq35_3 = TexMobject(
            r"| \cdot \frac{1}{\mu}"
        ).set_color(BLUE)

        eq35_3.next_to(eq35_2, RIGHT)
        self.play(Write(eq35_3))
        self.wait(2)
        self.play(FadeOut(eq35_3))

        eq36_1 = TexMobject(
            r"1=\frac{1}{\mu}\cdot",
            r"e^\eta",
            "-",
            r"e^\eta"
        ).set_color(YELLOW)
        eq36_1.set_color_by_tex_to_color_map({
            r"e^\eta": GREEN,
        })
        self.present_down_disappear_push_up(eq35_2, eq36_1)
        eq36_2 = TexMobject(
            r"1=",
            r"e^\eta",
            r"\cdot\left(\frac{1}{\mu}-1\right)",
        ).set_color(YELLOW)
        eq36_2.set_color_by_tex_to_color_map({
            r"e^\eta": GREEN,
        })
        self.present_down_disappear_push_up(eq36_1, eq36_2)
        self.wait(1)
        eq36_3 = TexMobject(
            r"| \cdot \frac{1}{e^\eta}"
        ).set_color(GREEN)
        eq36_3.next_to(eq36_2, RIGHT)

        self.play(FadeIn(eq36_3))
        self.wait(2)
        self.play(FadeOut(eq36_3))

        eq37_1 = TexMobject(
            r"\frac{1}{e^\eta}=\frac{1}{\mu}",
            r"-1",
        ).set_color(YELLOW)
        self.present_down_disappear_push_up(eq36_2, eq37_1)
        eq37_1.set_color_by_tex_to_color_map({
            r"-1": GREEN,
        })
        eq37_2 = TexMobject(r"| + 1").set_color(GREEN)
        eq37_2.next_to(eq37_1, RIGHT)
        self.play(FadeInFromDown(eq37_2))
        self.wait(2)
        self.play(FadeOut(eq37_2))

        eq38_1 = TexMobject(
            r"\frac{1}{e^\eta}+1=\frac{1}{\mu}"
        ).set_color(YELLOW)

        self.present_down_disappear_push_up(eq37_1, eq38_1)

        eq39_1 = TexMobject(
            r"\frac{1}{e^{-\eta}+1}=\mu=\mu(\eta)"
        ).set_color(YELLOW)

        self.present_down_disappear_push_up(eq38_1, eq39_1)
        eq39_1.next_to(eq28_5, DOWN)
        self.play(ApplyMethod(eq38_1.align_to, eq27_1, LEFT))
        self.add(eq39_1)
        self.play(ApplyMethod(eq39_1.align_to, eq27_1, LEFT))


    def draw_40_to_44(self):
        eq26_1 = TexMobject(
            r"{N\choose x}",
            r"\cdot",
            r"exp \left(x \cdot ln \left(\frac{\mu}{1-\mu} \right) \right)",
            r"\cdot",
            r"exp \left(N \cdot ln(1-\mu) \right)"
        ).set_color_by_tex_to_color_map({
            r"{N\choose x}": RED,
        })

        eq27_1 = TexMobject(
            r"{N\choose x}",
            r"\cdot",
            r"exp(N \cdot ln(1-\mu))",
            r"\cdot",
            r"exp\left(",
            r"ln\left(\frac{\mu}{1-\mu}\right)",
            r"\cdot x",
            r"\right)"
        ).set_color_by_tex_to_color_map({
            r"exp(N \cdot ln(1-\mu))": GREEN,
            r"exp\left(ln\left(\frac{\mu}{1-\mu}\right) \cdot x\right)": BLUE,
        })

        self.present_down_disappear_push_up(eq26_1, eq27_1)
        eq27_1.shift(2*UP)
        eq27_1.set_color(WHITE)
        self.add(eq27_1)

        eq28_1 = TexMobject(r"h(x)")
        eq28_1.next_to(eq27_1, DOWN)
        eq28_1.align_to(eq27_1, LEFT)
        eq28_2 = TexMobject(r"\cdot")
        eq28_2.next_to(eq28_1, RIGHT)
        eq28_3 = TexMobject(r"g(",r"\eta",")")
        eq28_3.next_to(eq28_2, RIGHT)
        eq28_4 = TexMobject(r"\cdot")
        eq28_4.next_to(eq28_3, RIGHT)
        eq28_5 = TexMobject(
            r"e^{(",
            r"\eta^T",
            r"\cdot", "u(x)", ")}",
        )
        eq28_5.next_to(eq28_4, RIGHT)

        self.add(eq28_1)
        self.add(eq28_2)
        self.add(eq28_3)
        self.add(eq28_4)
        self.add(eq28_5)


        eq39_1 = TexMobject(
            r"\frac{1}{e^{-\eta}+1}=\mu=\mu(\eta)"
        ).set_color(YELLOW)

        # self.present_down_disappear_push_up(eq38_1, eq39_1)
        eq39_1.next_to(eq28_5, DOWN)
        self.play(ApplyMethod(eq38_1.align_to, eq27_1, LEFT))
        self.add(eq39_1)

        self.play(ApplyMethod(eq39_1.align_to, eq27_1, LEFT))

                #
        eq28_3.set_color_by_tex_to_color_map({
            "g(": BLUE_D,
            ")": BLUE_D,
        })

        eq40_1 = TexMobject(
            r"g(\eta)=exp(N \cdot ln(1-",
            r"\mu",
            r"))",
        ).set_color(BLUE_D)
        eq40_1.set_color_by_tex_to_color_map({
            r"\mu": YELLOW,
        })
        eq40_1.next_to(eq39_1, DOWN)
        eq40_1.align_to(eq27_1, LEFT)
        self.play(FadeInFromDown(eq40_1))

        eq40_2 = TexMobject(
            r"g(\eta)=exp(N \cdot ln(1-",
            r"\mu(\eta)",
            ")",
        ).set_color(BLUE_D)
        eq40_2.set_color_by_tex_to_color_map({
            r"\mu(\eta)": YELLOW,
        })

        self.present_down_disappear_push_up(eq40_1, eq40_2)

        eq28_5.set_color_by_tex_to_color_map({
            "u(x)": GREEN,
        })

        eq42_1 = TexMobject(
            r"u(x)=x"
        ).set_color(GREEN)

        eq42_1.next_to(eq40_2, DOWN)
        eq42_1.align_to(eq27_1, LEFT)
        self.play(FadeInFromDown(eq42_1))

        eq44_0 = TexMobject(
            r"= p(x|\eta)"
        )
        eq44_0.next_to(eq28_5, RIGHT)
        self.play(FadeIn(eq44_0))
        eq44_1 = TexMobject(
            r"p(x|\eta)=",
            r"{N \choose x}",
            r"\cdot",
            r"exp\left(N \cdot ln\left(1-\mu(\eta) \right)\right)",
            r"\cdot",
            r"exp(",
            r"\mu(\eta)",
            r"\cdot x",
            ")"
        )

        eq44_1.next_to(eq42_1, DOWN)
        eq44_1.align_to(eq27_1, LEFT)
        self.play(FadeInFromDown(eq44_1))
        eq44_1.set_color_by_tex_to_color_map({
            r"p(x|\eta)=": WHITE,
        })
        self.wait(3)
        eq28_1.set_color(WHITE)
        eq28_3.set_color(WHITE)
        eq28_5.set_color(WHITE)
        self.wait(3)
        eq28_5.set_color_by_tex_to_color_map({
            r"\eta^T": RED,
        })
        self.wait(3)
        eq27_1.set_color_by_tex_to_color_map({
            r"ln\left(\frac{\mu}{1-\mu}\right)": RED,
        })
        self.wait(3)
        eq44_1.set_color_by_tex_to_color_map({
            r"\mu(\eta)": RED,
        })
        eq44_1.set_color_by_tex_to_color_map({
            r"exp\left(N \cdot ln\left(1-\mu(\eta) \right)\right)": WHITE,
        })

        self.wait(3)
        eq28_1.set_color(GREY)
        self.wait(3)
        eq27_1.set_color_by_tex_to_color_map({
            r"{N\choose x}": GREY,
        })
        self.wait(3)
        eq44_1.set_color_by_tex_to_color_map({
            r"{N \choose x}": GREY,
        })
        self.wait(3)

        eq28_3.set_color(BLUE)
        self.wait(3)
        eq27_1.set_color_by_tex_to_color_map({
            r"exp(N \cdot ln(1-\mu))": BLUE,
        })
        self.wait(3)
        eq44_1.set_color_by_tex_to_color_map({
            r"exp\left(N \cdot ln\left(1-\mu(\eta) \right)\right)": BLUE,
        })
        self.wait(3)

        eq28_5.set_color_by_tex_to_color_map({
            "u(x)": GREEN,
        })
        self.wait(3)
        eq27_1.set_color_by_tex_to_color_map({
            r"\cdot x": GREEN,
        })
        self.wait(3)
        eq44_1.set_color_by_tex_to_color_map({
            r"\cdot x": GREEN,
        })

        # eq6_geta = TexMobject(r"g(\eta) = 1 - \sigma(\eta)", r"= \sigma(-\eta)", r"\text{ with } \mu =", r" \sigma(\eta) = \frac{1}{1+e^{-\eta}}").set_color(BLUE)


    def construct(self):
        self.draw_3_to_10()

        self.wait(5)

    def present_down_disappear_push_up(self, eq1, eq2):
        eq2.next_to(eq1, DOWN)
        eq2.align_to(eq1, LEFT)
        self.play(Write(eq2))
        self.wait(2)
        self.play(FadeOut(eq1))
        self.play(ApplyMethod(eq2.shift, UP))
        self.wait(2)

class ColoringEquations(Scene):
    # Grouping and coloring parts of equations
    def construct(self):
        line1 = TexMobject(
            r"\text{The vector } \vec{F}_{net} \text{ is the net }",
            r"\text{force }",
            r"\text{on object of mass }",
        )
        line1.set_color_by_tex("force", BLUE)
        line2 = TexMobject(
            "m", "\\text{ and acceleration }", "\\vec{a}", ".  "
        )
        line2.set_color_by_tex_to_color_map({"m": YELLOW, "{a}": RED})
        sentence = VGroup(line1, line2)
        sentence.arrange_submobjects(DOWN, buff=MED_LARGE_BUFF)
        self.play(Write(sentence))

class UsingBraces(Scene):
    #Using braces to group text together
    def construct(self):
        eq1A = TextMobject("4x + 3y")
        eq1B = TextMobject("=")
        eq1C = TextMobject("0")
        eq2A = TextMobject("5x -2y")
        eq2B = TextMobject("=")
        eq2C = TextMobject("3")
        eq1B.next_to(eq1A,RIGHT)
        eq1C.next_to(eq1B,RIGHT)
        eq2A.shift(DOWN)
        eq2B.shift(DOWN)
        eq2C.shift(DOWN)
        eq2A.align_to(eq1A,LEFT)
        eq2B.align_to(eq1B,LEFT)
        eq2C.align_to(eq1C,LEFT)

        eq_group=VGroup(eq1A,eq2A)
        braces=Brace(eq_group,LEFT)
        eq_text = braces.get_text("A pair of equations")

        self.add(eq1A, eq1B, eq1C)
        self.add(eq2A, eq2B, eq2C)
        self.play(GrowFromCenter(braces),Write(eq_text))
